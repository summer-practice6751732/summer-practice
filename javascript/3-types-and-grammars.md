# Types & Grammars

## Types

- A Type by Any Other Name…
- Built-in Types
- Values as Types
  - undefined Versus “undeclared”
  - typeof Undeclared

## Values

- Arrays
  - Array-Likes
- Strings
- Numbers
  - Numeric Syntax
  - Small Decimal Values
  - Safe Integer Ranges
  - Testing for Integers
  - 32-Bit (Signed) Integers
- Special Values
  - The Nonvalue Values
  - Undefined
  - Special Numbers
  - Special Equality
- Value Versus Reference

## Natives

- Internal \[\[Class\]\]
- Boxing Wrappers
  - Object Wrapper Gotchas
- Unboxing
- Natives as Constructors
  - Array(..)
  - Object(..), Function(..), and RegExp(..)
  - Date(..) and Error(..)
  - Symbol(..)
  - Native Prototypes

## Coercion

- Converting Values
- Abstract Value Operations
  - ToString
  - ToNumber
  - ToBoolean
- Explicit Coercion
  - Explicitly: Strings <--> Numbers
  - Explicitly: Parsing Numeric Strings
  - Explicitly: \* --> Boolean
- Implicit Coercion
  - Simplifying Implicitly
  - Implicitly: Strings <--> Numbers
  - Implicitly: Booleans --> Numbers
  - Implicitly: \* --> Boolean
  - Operators || and &&
  - Symbol Coercion
- Loose Equals Versus Strict Equals
  - Equality Performance
  - Abstract Equality
  - Edge Cases
- Abstract Relational Comparison

## Grammar

- Statements & Expressions
  - Statement Completion Values
  - Expression Side Effects
  - Contextual Rules
- Operator Precedence
  - Short Circuited
  - Tighter Binding
  - Associativity
  - Disambiguation
- Automatic Semicolons
  - Error Correction
- Errors
  - Using Variables Too Early
- Function Arguments
- try..finally
- switch

## Mixed Environment JavaScript

- Annex B (ECMAScript)
  - Web ECMAScript
- Host Objects
- Global DOM Variables
- Native Prototypes
  - Shims/Polyfills
- \<script\>s
- Reserved Words
- Implementation Limits
