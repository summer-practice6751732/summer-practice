## JavaScript

- JIT Compilation
- First-class Function
- Prototype-based
- Single-threaded
- Dynamic language
  - Runtime object creation
  - Variable parameter list
  - Dynamic script creation (via eval)
  - Object introspection (via for...in and Object utilities)
  - Source-code recovery (via toString function's method)
- Multi-paradigm (OOP, imperative, and declarative styles) - code structure

<!--  -->

- Standard

  - Javascript conforms to the ECMAScript specification

    - ECMAScript specification is a blueprint for creating a scripting language
    - ECMAScript Language Specification (ECMA-262)

  - Releases

    - ES5 (ECMAScript)
    - ES6 (ES2015) - biggest release
      - classes
      - ES Modules
      - arrow function
      - let, const
      - promises
      - ...
    - ...
    - ES14 (ES2023)

<!--  -->

- Where it runs?

  - Browser
  - Server (NodeJS, Cloudflare Workers, Deno)
  - Mobile (React Native, Ionic)
  - Desktop Applications (Electron)

<!--  -->

- Resources

  - https://developer.mozilla.org/en-US/docs/Web/JavaScript
  - https://262.ecma-international.org/6.0/
  - https://262.ecma-international.org/14.0/
  - https://developers.cloudflare.com/workers/learning/how-workers-works/
